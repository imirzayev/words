from django.db import models


# Create your models here.
class Category(models.Model):
    category_name = models.CharField(max_length=100)

    def __str__(self):
        return self.category_name


class Word(models.Model):
    WordType = (
        ('N', 'Noun'),
        ('Adj', 'Adjective'),
        ('V', 'Verb'),
        ('Adv', 'Adverb'),
        ('P', 'Pronoun'),
        ('Num', 'Numeral'),
    )
    StatusType = (
        ('E', 'Excellent'),
        ('G', 'Good'),
        ('P', 'Poor')
    )

    word = models.CharField(max_length=100)
    definition = models.TextField()
    category = models.ForeignKey(Category, null=True, blank=True, on_delete=models.CASCADE)
    type = models.CharField(blank=True, null=True, max_length=3, choices=WordType)
    status = models.CharField(max_length=1, blank=True, null=True, choices=StatusType, default='P')

    def __str__(self):
        return self.word
