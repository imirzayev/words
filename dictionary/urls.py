from django.urls import path
from .views import *


app_name = 'api'

urlpatterns = [
    path('categories/', category_list, name='category_list'),
    path('category/create/', category_create, name='category_create'),
    path('category/detail/<int:pk>/', category_detail, name='category_detail'),
    path('category/update/<int:pk>/', category_update, name='category_update'),
    path('category/update/<int:pk>/', category_update, name='category_update'),
    path('category/delete/<int:pk>/', category_delete, name='category_delete'),
    path('words/', word_list, name='word_list'),
    path('word/create/', word_create, name='word_create'),
    path('word/detail/<int:pk>/', word_detail, name='word_detail'),
    path('word/status_update/<int:pk>/', word_status_update, name='word_status_update'),
    path('word/update/<int:pk>/', word_update, name='word_update'),
    path('word/delete/<int:pk>/', word_delete, name='word_delete'),
]