from django.shortcuts import render
from dictionary.models import Category


# Create your views here.
def single_view(request):
    return render(request, 'frontend/single.html')


def words_view(request):
    return render(request, 'frontend/words.html')


def create_view(request):
    categories = Category.objects.all()
    return render(request, 'frontend/create.html', {'categories': categories})
