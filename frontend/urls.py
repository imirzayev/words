from django.urls import path
from .views import *


app_name = 'frontend'
urlpatterns = [
    path('', single_view, name='single_word'),
    path('words/', words_view, name='words'),
    path('create/', create_view, name='create')
]