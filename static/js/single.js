let words_list_url = '/api/words/'
let words_status_update_url = '/api/word/status_update/'
let word_body = document.getElementById('container')
let word_order = 0
let words = {}

getWordsList('all')
    .then(data=>{
        words = data
        console.log(data)
        populateWord(data, word_order)
    })

async function getWordsList(category) {
    try {
        const response = await fetch(words_list_url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrftoken
            },
            body: JSON.stringify(category),
        })
        return response.json()
    } catch (errors) {
        console.log(`error ${errors}`)
    }
}

async function updateWordStatus(id, status) {
    let url = words_status_update_url + id + "/"
    try {
        const response = await fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrftoken
            },
            body: JSON.stringify({'status': status}),
        })
        return response.json()
    } catch (errors) {
        console.log(`error ${errors}`)
    }
}


function populateWord(data, i) {
    word_body.innerHTML = ''
    let counter = parseInt(i) + 1
    let box = `<div class="word-container">
                <div class="word">
                    <a class="word-link" data-id="" href="#"><h4 class="word-name">${counter}. ${data[i].word}</h4></a>
                </div>
                <div class="buttons">
                    <button class="btn status ${data[i].status === 'P' ? "btn-danger" : "btn-outline-danger"}" data-id="${data[i].id}" data-status="P"><i class="fa fa-frown-o"></i></button>
                    <button class="btn status ${data[i].status === 'G' ? "btn-warning" : "btn-outline-warning"}" data-id="${data[i].id}" data-status="G"><i class="fa fa-meh-o"></i></button>
                    <button class="btn status ${data[i].status === 'E' ? "btn-success" : "btn-outline-success"}" data-id="${data[i].id}" data-status="E"><i class="fa fa-smile-o"></i></button>
                </div>
            </div>
            <div class="collapse-button-container">
            <button class="btn" id="info-button" data-id=""><i id="info-icon" class="fa fa-angle-double-down"></i></button>
            </div>
            <div class="word-info" style="display: none;">
                <div class="category-info">
<!--                   ${data[i].category ? `<span>Category: ${data[i].category}</span>` : ""}-->
<!--                   ${data[i].type ? `<span>Type: ${data[i].type}</span>` : ""}-->
                </div>
                <div class="word-definition">
                    <span>Definition: ${data[i].definition}</span>
                </div>
            </div>
            <div class="change-buttons">
            ${word_order === 0 ? "" : "<button class=\"btn\" id=\"previous\" data-id=\"\"><i class=\"fa fa-angle-left\"></i></button>"}
            ${word_order !== (data.length - 1) ? "<button class=\"btn\" id=\"next\" data-id=\"\"><i class=\"fa fa-angle-right\"></i></button>" : ""}
            </div>`
    word_body.innerHTML += box

    document.getElementById('info-button').addEventListener('click', function (){
        let x = document.getElementsByClassName('word-info')[0]
        let y = document.getElementById('info-icon')
        if (x.style.display === "none") {
            x.style.display = "flex";
            y.classList.remove('fa-angle-double-down')
            y.classList.add('fa-angle-double-up')
        } else {
            x.style.display = "none";
            y.classList.remove('fa-angle-double-up')
            y.classList.add('fa-angle-double-down')
        }
    })

    if(document.getElementById('next')) {
        document.getElementById('next').addEventListener('click', function () {
            word_order++
            populateWord(words, word_order)
        })
    }

    if(document.getElementById('previous')) {
        document.getElementById('previous').addEventListener('click', function () {
            word_order--
            populateWord(words, word_order)
        })
    }

    let statusButtons = document.getElementsByClassName('status')
    for(let i = 0; i < statusButtons.length; i++) {
        statusButtons[i].addEventListener('click', function (){
            // console.log(this.dataset.status)
            let id = this.dataset.id
            let status = this.dataset.status

            updateWordStatus(id, status)
                .then(data => {
                    words[word_order] = data
                    populateWord(words, word_order)
                })
        })
    }
}


