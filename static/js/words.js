let words_list_url = '/api/words/'
let table_body = document.getElementById('table-body')

getWordsList('all')
    .then(data=>{
        console.log(data)
        populateWords(data)
    })

async function getWordsList(category) {
    try {
        const response = await fetch(words_list_url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrftoken
            },
            body: JSON.stringify(category),
        })
        return response.json()
    } catch (errors) {
        console.log(`error ${errors}`)
    }
}


function populateWords(data) {
    table_body.innerHTML = ''
    for (let i in data) {
        let counter = parseInt(i) + 1
        let word = `<tr style="cursor: pointer" onclick="console.log('pesi')">
                <th scope="row">${counter}</th>
                <td>${data[i].word}</td>
                <td>${data[i].definition}</td>
            </tr>`
        table_body.innerHTML += word
    }
}