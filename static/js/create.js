let word_create_url = '/api/word/create/'
let submitButton = document.getElementById('create-task')
let wordForm = document.getElementById('word-form')



submitButton.addEventListener('click', function (e){
    e.preventDefault()
    let word = document.getElementById('word-input').value
    let definition = document.getElementById('definition-textarea').value
    let type = document.getElementById('type-input').value
    let category = document.getElementById('category-input').value

    let data = {
        'word': word,
        'definition': definition,
        'type': type,
        'category': category
    }

    console.log(data)
    stop()

    createWord(data)
        .then(data=> {
            console.log(data)
            document.getElementById("type-input").selectedIndex = 0;
            document.getElementById("category-input").selectedIndex = 0;
            wordForm.reset()
            alert('added')
        })
})


async function createWord(data) {
    try {
        const response = await fetch(word_create_url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrftoken
            },
            body: JSON.stringify(data),
        })
        return response.json()
    } catch (errors) {
        console.log(`error ${errors}`)
    }
}