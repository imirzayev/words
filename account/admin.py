from django.contrib import admin
from django import forms
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from .models import Account


# Register your models here.
class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = Account
        fields = ('user_name', )

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Account
        fields = ('user_name', 'email', 'password', 'is_active', 'is_admin', 'qr_code', 'profile_photo', 'cover_photo')

    def clean_password(self):
        return self.initial["password"]



class UserAdmin(BaseUserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = ('user_name', 'qr_code', 'email', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        ('Credentials', {'fields': ('user_name', 'password', 'qr_code')}),
        ('Personal Information', {'fields': ('email', 'profile_photo', 'cover_photo', )}),
        ('Permissions', {'fields': ('is_admin', )}),
    )
    add_fieldsets = (
        (None, {'classes': ('wide', ), 'fields': ('user_name', 'password1', 'password2'), }),
    )

    search_fields = ('user_name', )
    ordering = ('user_name', )
    filter_horizontal = ()

    readonly_fields = ['qr_code']


admin.site.register(Account, UserAdmin)
admin.site.unregister(Group)
