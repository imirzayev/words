from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils.crypto import get_random_string


# Custom User Manager
class AccountManager(BaseUserManager):
    def create_user(self, user_name, password=None):
        if not user_name:
            raise ValueError('Users must have username!')

        user = self.model(
            user_name=user_name
        )

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, user_name, password=None):
        user = self.create_user(user_name, password)

        user.is_admin = True

        user.save(using=self._db)

        return user


# Custom User Model
class Account(AbstractBaseUser):
    user_name = models.CharField(max_length=100, unique=True, verbose_name='Username')
    email = models.EmailField(max_length=255, verbose_name='Email Address')
    profile_photo = models.ImageField(upload_to='profile', blank=True, null=True)
    cover_photo = models.ImageField(upload_to='cover', blank=True, null=True)

    qr_code = models.CharField(max_length=12, unique=True, default=get_random_string, verbose_name='QR Code')

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'user_name'
    EMAIL_FIELD = 'email'

    objects = AccountManager()

    def __str__(self):
        return self.user_name

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

